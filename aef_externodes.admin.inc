<?php


function external_source_views_form()
{
  if(module_exists('views') == false)
    return '';

  $aef_externodes_views_settings = variable_get('aef_externodes_views', array());

  $form['#tree'] = true;

  //Prepare the source options
  $es_options = array(0 => t('Local Drupal database'));
  $sources = aef_external_sources_get_all();
  foreach($sources as $esid => $source)
  {
    $es_options[$esid] = t('Remote source: ') . $source['infos']['name'];
  }

  $views = views_get_all_views();
  foreach($views as $view_name => $view)
  {
 
    foreach($view->display as $display_name => $display)
    {
      $form['aef_externodes_views'][$view_name][$display_name] = array(
        '#type' => 'select',
        '#options' => $es_options,
        '#title' => t('View %view_name - display %display_name', array('%view_name' => $view_name, '%display_name' => $display_name)),
        '#default_value' => $aef_externodes_views_settings[$view_name][$display_name],
      );
    }
  }

  return system_settings_form($form);
}
