<?php

/**
 * Implementation of hook_init()
 */
function aef_externodes_init()
{
  //Override the necessary Drupal core function... If the runkit apache module exists!
  if(function_exists('runkit_function_redefine'))
  {
    module_load_include('inc', 'aef_externodes', 'aef_externodes.overriden_functions');
    aef_externodes_override_functions();
  }
}

/**
 * Implementation of hook_perm().
 */
function aef_externodes_perm() {
  return array('administer aef_externodes');
}

/**
 * Implementation of hook_menu().
 */
function aef_externodes_menu() {
  $items['admin/build/external_source_views'] = array(
    'title' => 'External sources for Views',
    'access arguments' => array('administer aef_externodes'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('external_source_views_form'),
    'description' => 'Tells Views to use an external source.',
    'file' => 'aef_externodes.admin.inc',
  );
  return $items;
}

/**
 * Implementation of hook_flush_caches().
 */
function aef_externodes_flush_caches() {
  return array('cache_aef_externodes_data');
}

/**
 * Implementation of hook_form_alter()
 */
function aef_externodes_form_alter(&$form, $form_state, $form_id)
{
  if($form['#id'] == 'aef-external-sources-add-form')
  {
    $form['data']['aef_externodes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node and File mapping'),
    );

    $form['data']['aef_externodes']['enable'] = array(
      '#type' => 'select',
      '#title' => t('Activate nid/fid address space mapping to this external source.'),
      '#options' => array(
          'yes' => t('Yes'),
          'cache_only' => t('Yes, but only serve cached node/files, do not make remote calls.'),
          'no' => t('No'),
        ),
      '#description' => t('Enable/disable this services. The third option is useful when you remote server is down and you need to skip the timeout wait.'),
      '#default_value' => $form['#data']['aef_externodes']['enable'],
    );

    $form['data']['aef_externodes']['nid_begin'] = array(
      '#type' => 'textfield',
      '#title' => t('Beginning of nid mapping area'),
      '#description' => t('Referenced nodes with a nid in the mapping area will in fact reference nodes from this external source.'),
      '#size' => '10',
      '#default_value' => $form['#data']['aef_externodes']['nid_begin'],
    );

    $form['data']['aef_externodes']['nid_end'] = array(
      '#type' => 'textfield',
      '#title' => t('End of nid mapping area'),
      '#description' => t('Referenced nodes with a nid in the mapping area will in fact reference nodes from this external source.'),
      '#size' => '10',
      '#default_value' => $form['#data']['aef_externodes']['nid_end'],
    );

    $form['data']['aef_externodes']['fid_begin'] = array(
      '#type' => 'textfield',
      '#title' => t('Beginning of fid mapping area'),
      '#description' => t('Referenced files with a fid in the mapping area will in fact reference files from this external source.'),
      '#size' => '10',
      '#default_value' => $form['#data']['aef_externodes']['fid_begin'],
    );

    $form['data']['aef_externodes']['fid_end'] = array(
      '#type' => 'textfield',
      '#title' => t('End of fid mapping area'),
      '#description' => t('Referenced files with a fid in the mapping area will in fact reference files from this external source.'),
      '#size' => '10',
      '#default_value' => $form['#data']['aef_externodes']['fid_end'],
    );

    $form['data']['aef_externodes']['caching'] = array(
      '#type' => 'checkbox',
      '#title' => t('Activate local caching of nodes and file infos.'),
      '#description' => t('Fetched nodes and file infos (not the file itself) will be stored locally in a cache. That will save you if the server is going down.'),
      '#default_value' => $form['#data']['aef_externodes']['caching'],
    );


    $form['data']['aef_externodes_taxonomy'] = array(
      '#type' => 'fieldset',
      '#title' => t('Taxonomy settings'),
    );

    $form['data']['aef_externodes_taxonomy']['taxonomy_sync'] = array(
      '#type' => 'checkbox',
      '#title' => t('Sync taxonomy changes'),
      '#description' => t('Whenever taxonomy change on this server, repercute the change on the remote server. Useful when making remote Views with taxonomy arguments.'),
      '#default_value' => $form['#data']['aef_externodes_taxonomy']['taxonomy_sync'],
    );

    $form['data']['aef_externodes_taxonomy']['taxonomy_sync_now'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete all taxonomy and sync taxonomy changes from scratch now'),
      '#default_value' => 0,
    );

    $form['#submit'][] = 'aef_externodes_source_add_form_submit';
  }
  else if ($form_id == 'node_type_form' && isset($form['#node_type'])) {
    // .../admin/content/node-type/xx page
    $node_type = $form['#node_type']->type;
    $node_creation_source = variable_get('aef_externodes_node_creation_source_' . $node_type, 0);

    $options_sources = array(0 => t('Local drupal database'));
    $sources = aef_external_sources_get_all();
    foreach($sources as $esid => $source)
    {
      $options_sources[$esid] = t('Remote source: ') . $source['infos']['name'];
    }

    // The code below borrows heavily from nodereference_field_settings()
    $form['aef_externodes_node_creation_config'] = array(
      '#type' => 'fieldset',
      '#title' => t('AEF Externodes'),
      '#collapsible' => TRUE,
      '#collapsed' => !$node_creation_source,
    );
    
    $form['aef_externodes_node_creation_config']['aef_externodes_node_creation_source'] = array(
      '#title' => t('Creation location'),
      '#description' => t('Location where nodes of this type will be created'),
      '#type' => 'select',
      '#options' => $options_sources,
      '#default_value' => $node_creation_source,
    );
  }
}

function aef_externodes_source_add_form_submit($form, &$form_state)
{
  $values = $form_state['values'];

  if($values['data']['aef_externodes_taxonomy']['taxonomy_sync_now'])
  {
    unset($values['data']['aef_externodes_taxonomy']['taxonomy_sync_now']);

    //Delete all terms on the remote serv, and sync with the current serv
    $data = taxonomy_get_vocabularies();
    foreach($data as $vid => $voc)
    {
      $data[$vid] = (array)$data[$vid];
      $data[$vid]['terms'] = taxonomy_get_tree($vid);
    }
    $xmlrpc_result = aef_external_sources_call_xmlrplc('taxonomy.eraseAndInstallAll', $values['esid'], $data);
  }

}

/**
 * For a given nid, return an esid (External source Id) if any.
 */
function aef_externodes_get_esid_from_nid($nid)
{
  $result = null;

  $sources = aef_external_sources_get_all();
  foreach($sources as $esid => $source)
  {
    if(isset($source['aef_externodes']) && $source['aef_externodes']['nid_begin'] > 0)
    {
      if($source['aef_externodes']['nid_begin'] <= $nid && 
        $nid <= $source['aef_externodes']['nid_end'])
      {
        $result = $esid;
        break;
      }
    }
  }
  return $result;
}

/**
 * For a given fid, return an esid (External source Id) if any.
 */
function aef_externodes_get_esid_from_fid($fid)
{
  $result = null;

  $sources = aef_external_sources_get_all();
  foreach($sources as $esid => $source)
  {
    if(isset($source['aef_externodes']) && $source['aef_externodes']['fid_begin'] > 0)
    {
      if($source['aef_externodes']['fid_begin'] <= $fid && 
        $fid <= $source['aef_externodes']['fid_end'])
      {
        $result = $esid;
        break;
      }
    }
  }
  return $result;
}

/**
 * Addition to node_load
 */
function aef_externodes_node_load($param = array(), $revision = NULL, $reset = NULL)
{
  $result = null;

  if(is_numeric($param))
  {
    $esid = aef_externodes_get_esid_from_nid($param);

    //If esid > 0, the node is in an external source
    if($esid > 0)
    {
      $es_infos = aef_external_sources_load($esid);
      if($es_infos != null)
      {
        $mapping_infos = $es_infos['aef_externodes'];
        $remote_nid = (int)($param - $mapping_infos['nid_begin']);

        if($mapping_infos['enable'] != "no")
        {
          //Get the cached result, if asked
          $cache_entry = null;
          if($mapping_infos['caching'])
            $cache_entry = cache_get('esid:' . $esid . ':nid:' . $remote_nid, 'cache_aef_externodes_data');

          if($cache_entry->data != null)
            $result = $cache_entry->data;
          else if($mapping_infos['enable'] != "cache_only")
          {
            //Make the XMLRPC call
            $xmlrpc_result = aef_external_sources_call_xmlrplc('node.get', $esid, $remote_nid, array());
            if ($xmlrpc_result != null) 
            {
              $node = $xmlrpc_result;

              //Look at the node, add a mask to nids and fids 
              foreach($node as $key => $value)
              {
                if(substr($key, 0, 6) == "field_" && is_array($node[$key]))
                {
                  foreach($node[$key] as $delta => $value)
                  {
                    if(isset($node[$key][$delta]['nid']) && $node[$key][$delta]['nid'] > 0)
                      $node[$key][$delta]['nid'] += $mapping_infos['nid_begin'];
                    if(isset($node[$key][$delta]['fid']) && $node[$key][$delta]['fid'] > 0)
                      $node[$key][$delta]['fid'] += $mapping_infos['fid_begin'];
                  }
                }
                else if($key == "feedapi_node")
                {
                  if(isset($node[$key]['nid']))
                    $node[$key]['nid'] += $mapping_infos['nid_begin'];
                  if(is_array($node[$key]['feed_nids']))
                  {
                    $new_feed_nids = array();
                    foreach($node[$key]['feed_nids'] as $nid)
                      $new_feed_nids[$nid + $mapping_infos['fid_begin']] = $nid + $mapping_infos['fid_begin'];
                    $node[$key]['feed_nids'] = $new_feed_nids;
                  }
                  //Make it a object
                  $node[$key] = (object)$node[$key];
                }
                else if($key == "feed")
                {
                  if(isset($node[$key]['nid']))
                    $node[$key]['nid'] += $mapping_infos['nid_begin'];
                  $node[$key] = (object)$node[$key];
                }
                else if($key == "taxonomy")
                {
                  if(is_array($node[$key]))
                  {
                    foreach($node[$key] as $tid => $term)
                    {
                      $node[$key][$tid] = (object)$node[$key][$tid];
                    }
                  }
                }
              }

              $node = (object)$node;

              //Let other modules alter this externode (updated fid, nid, ...), like we did just above
              $node->external_source_infos = $es_infos;
              $node->external_source_id = $esid;
              drupal_alter('externode_being_loaded', $node);

              //Update the value of nid (the xmlrpc callback returned the local value, not the mapped one)
              $remote_nid = $node->nid;
              $remote_vid = $node->vid;
              $node->nid += $mapping_infos['nid_begin'];
              $node->vid += $mapping_infos['nid_begin'];

              //Update the node_access table if not already done so before
              if(db_result(db_query("SELECT COUNT(nid) FROM {node_access} WHERE nid=%d", $node->nid)) == 0)
                node_access_acquire_grants($node);

              $result = $node;

              //Cache the result, if asked
              if($mapping_infos['caching'])
                cache_set('esid:' . $esid . ':nid:' . $remote_nid, $node, 'cache_aef_externodes_data');
            }
          }
        }
      }
    }
  }

  return $result;
}

/**
 * Implementation of hook_aef_image_fid_load()
 */
function aef_externodes_aef_image_fid_load(&$file, $fid)
{
  $esid = aef_externodes_get_esid_from_nid($fid);
  $es_infos = aef_external_sources_load($esid);
  $mapping_infos = $es_infos['aef_externodes'];

  //If esid > 0, the file is in an external source
  if($esid > 0)
  {
    $remote_fid = (int)($fid - $mapping_infos['fid_begin']);

    if($mapping_infos['enable'] != "no")
    {
      //Cache the result, if asked
      $cache_entry = null;
      if($mapping_infos['caching'])
        $cache_entry = cache_get('esid:' . $esid . ':fid:' . $remote_fid, 'cache_aef_externodes_data');

      if($cache_entry->data != null)
        $file = $cache_entry->data;
      else if($mapping_infos['enable'] != "cache_only")
      {
        $xmlrpc_result = aef_external_sources_call_xmlrplc('file.getwithoutdata', $esid, $remote_fid);
        if ($xmlrpc_result != null) {
          $file = $xmlrpc_result;

          //Cache the result, if asked
          if($mapping_infos['caching'])
            cache_set('esid:' . $esid . ':fid:' . $remote_fid, $file, 'cache_aef_externodes_data');
        }
      }
    }
  }
}


/**
 * Addition to node_save
 */
function aef_externodes_node_save(&$node)
{
  $success = false;
  $node_is_new = empty($node->nid);

  if($node == null)
    return $success;

  //If this node has an nid, check its mapping
  if(is_numeric($node->nid))
  {
    $esid = aef_externodes_get_esid_from_nid($node->nid);
  }
  //Else, check where is the creation location of this node
  else
  {
    $esid = variable_get('aef_externodes_node_creation_source_' . $node->type, 0);
  }

  //If esid > 0, the node is in an external source
  if($esid > 0)
  {
    $es_infos = aef_external_sources_load($esid);
    if($es_infos != null)
    {
      $mapping_infos = $es_infos['aef_externodes'];

      if($mapping_infos['enable'] == "yes")
      {
        //We have found that this node is mapped elsewhere: mark the loading as successful, 
        //even if the XMLRPC call fails
        $success = true;

        //Prepare the node
        //Look at the node, remove a mask to nids and fids 
        if($node->nid != 0)
        {
          $node->nid -= $mapping_infos['nid_begin'];
          $node->vid -= $mapping_infos['nid_begin'];
        }

        //Clear the cache
        if($mapping_infos['caching'])
          cache_set('esid:' . $esid . ':nid:' . $node->nid, null, 'cache_aef_externodes_data');

        //Remove other fids and nids masks
        $node = (array)$node;
        foreach($node as $key => $value)
        {
          if(substr($key, 0, 6) == "field_" && is_array($node[$key]))
          {
            foreach($node[$key] as $delta => $value)
            {
              if(isset($node[$key][$delta]['nid']) && $node[$key][$delta]['nid'] > 0)
                $node[$key][$delta]['nid'] -= $mapping_infos['nid_begin'];
              if(isset($node[$key][$delta]['fid']) && $node[$key][$delta]['fid'] > 0)
              {
                if($node[$key][$delta]['fid'] > $mapping_infos['fid_begin'])
                  $node[$key][$delta]['fid'] -= $mapping_infos['fid_begin'];
                else
                {
                  //This fid is not stored on the remote source. Send and save the file
                  $node[$key][$delta]['fid'] = aef_externodes_file_save_remote($esid, $node[$key][$delta]['fid']);
                }
              }
            }
          }
          else if($key == "feedapi_node")
          {
            if(isset($node[$key]->nid))
              $node[$key]->nid -= $mapping_infos['nid_begin'];
            if(is_array($node[$key]->feed_nids))
            {
              $new_feed_nids = array();
              foreach($node[$key]->feed_nids as $nid)
                $new_feed_nids[$nid - $mapping_infos['fid_begin']] = $nid - $mapping_infos['fid_begin'];
              $node[$key]->feed_nids = $new_feed_nids;
            }
          }
          else if($key == "feed")
          {
            if(isset($node[$key]->nid))
              $node[$key]->nid -= $mapping_infos['nid_begin'];
          }
          else if($key == "feedapi")
          {
            if(isset($node[$key]->feed_nid))
              $node[$key]->feed_nid -= $mapping_infos['nid_begin'];
          }
        }
        $node = (object)$node;
        $node->external_source_infos = $es_infos;
        $node->external_source_id = $esid;
        drupal_alter('externode_being_saved', $node);

        //Change the uid to 1, since we don't have a user mapping
        //TODO: Make a user synchronization, like taxonomy?
        $node->uid = 1;

        //Make the XMLRPC call
        $xmlrpc_result = aef_external_sources_call_xmlrplc('node.realsave', $esid, $node);
        if ($xmlrpc_result != null) 
        {
          $node->nid = $xmlrpc_result['nid']+ $mapping_infos['nid_begin'];

          //The saved node has been altered, fetch it
          //$updated_node = node_load($node->nid);

          // Call the node specific callback (if any).
          // The insert/update op are called on the server side. They shouldn't be replicated on the client (e.g.
          // CCK field save). So the client don't know when these nodes are updated.
          // So we create use a new op for modules that are aware of the remote-source-ness of nodes
          if ($node_is_new)
            $op = 'externodes_insert';
          else
            $op = 'externodes_update';
          //node_invoke($node, $op);
          node_invoke_nodeapi($node, $op);

          // Update the node access table for this node.
          node_access_acquire_grants($node);

          // Clear the page and block caches.
          cache_clear_all();
        }
      }
    }
  }

  return $success;
}

/**
 * Addition to node_delete.
 */
function aef_externodes_node_delete($node)
{
  $result = false;

  $esid = aef_externodes_get_esid_from_nid($node->nid);

  //If esid > 0, the node is in an external source
  if($esid > 0)
  {
    $es_infos = aef_external_sources_load($esid);
    if($es_infos != null)
    {
      $mapping_infos = $es_infos['aef_externodes'];

      if($mapping_infos['enable'] == "yes")
      {

        $remote_nid = (int)($node->nid - $mapping_infos['nid_begin']);

        //Make the XMLRPC call
        $xmlrpc_result = aef_external_sources_call_xmlrplc('node.delete', $esid, $remote_nid);

        $result = true;

        //Clear the cache
        if($mapping_infos['caching'])
          cache_set('esid:' . $esid . ':nid:' . $remote_nid, null, 'cache_aef_externodes_data');

        // Clear the page and block caches.
        cache_clear_all();

        // Remove this node from the search index if needed.
        if (function_exists('search_wipe')) {
          search_wipe($node->nid, 'node');
        }
        watchdog('content', '@type: deleted %title.', array('@type' => $node->type, '%title' => $node->title));

      }
    }
  }

  return $result;
}

/**
 * Addition to get_current_revision_id.
 */
function aef_externodes_get_current_revision_id($nid)
{
  $vid = false;

  $esid = aef_externodes_get_esid_from_nid((int)($nid - $mapping_infos['nid_begin']));

  //If esid > 0, the node is in an external source
  if($esid > 0)
  {
    $es_infos = aef_external_sources_load($esid);
    if($es_infos != null)
    {
      $mapping_infos = $es_infos['aef_externodes'];

      if($mapping_infos['enable'] == "yes")
      {
        //Make the XMLRPC call
        $xmlrpc_result = aef_external_sources_call_xmlrplc('module_grants.get_current_revision_id', $esid, (int)($nid - $mapping_infos['nid_begin']));
        if ($xmlrpc_result != null) 
        {
          $vid = $xmlrpc_result['vid'] + $mapping_infos['nid_begin'];
        }
      }
    }
  }

  return $vid;
}

/**
 * Implementation of hook_views_pre_render()
 */
function aef_externodes_views_pre_render(&$view)
{
  static $aef_externodes_views = array();

  //Load the preferences
  if(count($aef_externodes_views) == 0)
    $aef_externodes_views = variable_get('aef_externodes_views', array());

  $esid = $aef_externodes_views[$view->name][$view->current_display];
  if($esid > 0)
  {
    global $pager_page_array, $pager_total, $pager_total_items;

    $es_infos = aef_external_sources_load($esid);
    $mapping_infos = $es_infos['aef_externodes'];

    //Check if the table results are cached. If true, do not make a xmlrpc call
    if($mapping_infos['enable'] == "yes")
    {
      $cache_plugin = $view->display_handler->get_cache_plugin();
      if($cache_plugin)
      {
        $cache_plugin_cutoff = $cache_plugin->cache_expire('results');
        if($cache_plugin_cutoff && 
          $cached_results = cache_get($cache_plugin->get_results_key(), $cache_plugin->table)) 
        {
          if ($cache_plugin_cutoff && $cached_results->data['externodes_cached'] == true) 
          {
            //it's going to serve cached data we have already overriden. We're doing nothing.
            return;
          }
        }
      }
    }

    if($mapping_infos['enable'] == "yes" || $mapping_infos['enable'] == "cache_only")
    {
      //In all cases, reset the results, since we don't want to show local results if the remote call fails.
     $view->result = array();
    }

    if($mapping_infos['enable'] == "yes")
    {
      //Prepare the get params to give
      $get_params = $_GET;
      unset($get_params['q']);

      //Check the arguments: If a nid is given, remove the mask
      $arg_delta = 0;
      foreach($view->argument as $arg_id => $arg_data)
      {
        if(substr($arg_data->options['field'], -4) == "_nid")
        {
          $nids = explode('+', $view->args[$arg_delta]);
          if(is_numeric(current($nids)))
          {
            for($i = 0; $i < count($nids); $i++)
              $nids[$i] -= $mapping_infos['nid_begin'];
            $view->args[$arg_delta] = implode('+', $nids);
          }
        }
        $arg_delta++;
      }

      //Make the XMLRPC call
      $xmlrpc_result = aef_external_sources_call_xmlrplc('views.getresultsandinfos', $esid, 
        $view->name, 
        $view->current_display, 
        $view->args, 
        (int)((isset($view->pager['offset']))?$view->pager['offset']:-1), 
        (int)((isset($view->pager['items_per_page']))?$view->pager['items_per_page']:-1), 
        $get_params);

      if ($xmlrpc_result != null && is_array($xmlrpc_result["results"]))
      {
        foreach($xmlrpc_result["results"] as $result)
        {
          $node = (object)$result;
          if(isset($node->nid) && $node->nid > 0)
            $node->nid += $mapping_infos['nid_begin'];
          if(isset($node->node_nid) && $node->node_nid > 0)
            $node->node_nid += $mapping_infos['nid_begin'];
          if(isset($node->feedapi_nid) && $node->feedapi_nid > 0)
            $node->feedapi_nid += $mapping_infos['nid_begin'];
          $view->result[] = $node;
        }

        //Views use global variables for their pagers.... Update them.
        $pager_total_items[$view->pager['element']] = $xmlrpc_result['pager_total_items'];
        $pager_total[$view->pager['element']] = $xmlrpc_result['pager_total'];
        $pager_page_array[$view->pager['element']] = $xmlrpc_result['pager_page_array'];


        //If views cache activated, store our results here a la place of the views results
        if($cache_plugin_cutoff && $cached_results)
        {
          $cached_results->data['result'] = $view->result;
          $cached_results->data['total_rows'] = $view->total_rows;
          $cached_results->data['pager'] = $view->pager;
          $cached_results->data['externodes_cached'] = true;

          cache_set($cache_plugin->get_results_key(), $cached_results->data, $cache_plugin->table);
        }
      }
    }
  }
}

/**
 * Implementation of hook_taxonomy().
 * Sync taxonomy with remote instances.
 */
function aef_externodes_taxonomy($op, $type, $array = NULL)
{
  $data = aef_external_sources_get_all();
  foreach($data as $esid => $infos)
  {
    if($infos['aef_externodes_taxonomy']['taxonomy_sync'])
    {
      switch($type)
      {
        case 'term':
          switch($op)
          {
            case 'insert':
            case 'update':
              $xmlrpc_result = aef_external_sources_call_xmlrplc('taxonomy.saveTerm', $esid, $array);
            break;

            case 'delete':
              $xmlrpc_result = aef_external_sources_call_xmlrplc('taxonomy.deleteTerm', $esid, $array);
            break; 
          } 
        break;

        case 'vocabulary':
          switch($op)
          {
            case 'insert':
            case 'update':
              $xmlrpc_result = aef_external_sources_call_xmlrplc('taxonomy.saveVocabulary', $esid, $array);
            break;

            case 'delete':
              $xmlrpc_result = aef_external_sources_call_xmlrplc('taxonomy.deleteVocabulary', $esid, $array);
            break; 
          } 
        break;
      }
    }
  }
}

/**
 * Remotely save a file from a local fid.
 * Return a remote fid.
 */
function aef_externodes_file_save_remote($esid, $local_fid)
{              
  if ($file = db_fetch_array(db_query('SELECT * FROM {files} WHERE fid = %d', $local_fid))) 
  {
    //Load the file
    $filepath = file_create_path($file['filepath']);
    $binaryfile = fopen($filepath, 'rb');
    $file['file'] = base64_encode(fread($binaryfile, filesize($filepath)));
    fclose($binaryfile);

    //Remove the file directory path part, if any
    $dirpath = file_directory_path();
    $dirlen = strlen($dirpath);
    if (substr($file['filepath'], 0, $dirlen + 1) == $dirpath .'/') {
      $file['filepath'] = substr($file['filepath'], $dirlen + 1);
    }

    //Send the image to be saved on the remote server as a fid
    $xmlrpc_result = aef_external_sources_call_xmlrplc('file.save', $esid,
      $file['filepath'], $file['file']);

    if($xmlrpc_result != null)
    {
      return $xmlrpc_result['fid'];
    }
    else
    {
      //Fails
      return 0;
    }
  }
}

/**
 * Implementation of hook_nodeapi()
 */
function aef_externodes_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL)
{
  //On validate, nodereference check that the node exists in the local db...
  //We must remove the errors it sets on remote nodes.
  //This code is a bit hackish :s, but no choice if we want to not edit nodereference directly
  if($op == "validate")
  {
    $type_name = is_string($node) ? $node : (is_array($node) ? $node['type'] : $node->type);
    $type = content_types($type_name);
    $field_types = _content_field_types();

    //Get the form errors, for edition. Also remove all the messages generated by them, we will
    //set them back later
    $form_errors = form_set_error();
    form_set_error(null, '', true);
    $messages = drupal_get_messages('error');
    if(is_array($messages))
    {
      foreach($form_errors as $error_elt => $message)
      {
        $error_msg_pos = array_search($message, $messages['error']);
        unset($messages['error'][$error_msg_pos]);
      }
      if(is_array($messages['error']))
      {
        foreach($messages['error'] as $error_msg)
          drupal_set_message($error_msg, 'error');
      }
    }

    //Iterate through the nodereference, looking for externodes
    foreach ($type['fields'] as $field) {
      $items = isset($node->$field['field_name']) ? $node->$field['field_name'] : array();

      // Make sure AHAH 'add more' button isn't sent to the fields for processing.
      unset($items[$field['field_name'] .'_add_more']);

      $module = $field_types[$field['type']]['module'];
      if($module == "nodereference")
      {
        foreach ($items as $delta => $item) 
        {
          if(is_array($item)) 
          {
            //If esid > 0, the node is in an external source
            $esid = aef_externodes_get_esid_from_nid($item['nid']);
            if($esid > 0)
            {
              //Remove our error
              unset($form_errors[$item['_error_element']]);
            }
          }
        }
      }
    }
    //Save back the remaining form errors
    foreach($form_errors as $error_elt => $message)
      form_set_error($error_elt, $message);
  }
}

/**
 * Implementation of hook_elements()
 */
function aef_externodes_elements() {
  $type = array();

  //Add our processing for nodereference: if it is a externode, put its remote value
  $type['nodereference_autocomplete'] = array(
    '#process' => array('aef_externodes_nodereference_process'),
  );

  return $type;
}

function aef_externodes_nodereference_process($element) {

  if ($element['#type'] == "nodereference_autocomplete") 
  {
    //If esid > 0, the node is in an external source
    $esid = aef_externodes_get_esid_from_nid($element['#default_value']['nid']);
    if($esid > 0)
    {
      $node = node_load($element['#default_value']['nid']);
      $element['nid']['#default_value']['nid'] = $node->title . ' [nid:'. $element['#default_value']['nid'] .']';
    }
  }

  return $element;
}

