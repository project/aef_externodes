<?php


function aef_externodes_override_functions()
{
  //
  // Modification of the node_load code!
  //
  $modified_node_load_code =  '
  static $nodes = array();

  if ($reset) {
    $nodes = array();
  }

  $cachable = ($revision == NULL);
  $arguments = array();
  if (is_numeric($param)) {
    if ($cachable) {
      // Is the node statically cached?
      if (isset($nodes[$param])) {
        return is_object($nodes[$param]) ? drupal_clone($nodes[$param]) : $nodes[$param];
      }
    }
    $cond = \'n.nid = %d\';
    $arguments[] = $param;
  }
  elseif (is_array($param)) {
    // Turn the conditions into a query.
    foreach ($param as $key => $value) {
      $cond[] = \'n.\'. db_escape_table($key) ." = \'%s\'";
      $arguments[] = $value;
    }
    $cond = implode(\' AND \', $cond);
  }
  else {
    return FALSE;
  }

  //************************ BEGIN OF PATCH ****************************
  //First check if the given params are referencing a node mapped in an external source
  if($externode = aef_externodes_node_load($param, $revision, $reset))
  {
    $nodes[$externode->nid] = is_object($externode) ? drupal_clone($externode) : $externode;
    return $externode;
  }
  //************************ END OF PATCH ****************************


  // Retrieve a field list based on the site\'s schema.
  $fields = drupal_schema_fields_sql(\'node\', \'n\');
  $fields = array_merge($fields, drupal_schema_fields_sql(\'node_revisions\', \'r\'));
  $fields = array_merge($fields, array(\'u.name\', \'u.picture\', \'u.data\'));
  // Remove fields not needed in the query: n.vid and r.nid are redundant,
  // n.title is unnecessary because the node title comes from the
  // node_revisions table.  We\'ll keep r.vid, r.title, and n.nid.
  $fields = array_diff($fields, array(\'n.vid\', \'n.title\', \'r.nid\'));
  $fields = implode(\', \', $fields);
  // Rename timestamp field for clarity.
  $fields = str_replace(\'r.timestamp\', \'r.timestamp AS revision_timestamp\', $fields);
  // Change name of revision uid so it doesn\'t conflict with n.uid.
  $fields = str_replace(\'r.uid\', \'r.uid AS revision_uid\', $fields);

  // Retrieve the node.
  // No db_rewrite_sql is applied so as to get complete indexing for search.
  if ($revision) {
    array_unshift($arguments, $revision);
    $node = db_fetch_object(db_query(\'SELECT \'. $fields .\' FROM {node} n INNER JOIN {users} u ON u.uid = n.uid INNER JOIN {node_revisions} r ON r.nid = n.nid AND r.vid = %d WHERE \'. $cond, $arguments));
  }
  else {
    $node = db_fetch_object(db_query(\'SELECT \'. $fields .\' FROM {node} n INNER JOIN {users} u ON u.uid = n.uid INNER JOIN {node_revisions} r ON r.vid = n.vid WHERE \'. $cond, $arguments));
  }

  if ($node && $node->nid) {
    // Call the node specific callback (if any) and piggy-back the
    // results to the node or overwrite some values.
    if ($extra = node_invoke($node, \'load\')) {
      foreach ($extra as $key => $value) {
        $node->$key = $value;
      }
    }

    if ($extra = node_invoke_nodeapi($node, \'load\')) {
      foreach ($extra as $key => $value) {
        $node->$key = $value;
      }
    }
    if ($cachable) {
      $nodes[$node->nid] = is_object($node) ? drupal_clone($node) : $node;
    }
  }

  return $node;
';

  runkit_function_redefine('node_load', '$param = array(), $revision = NULL, $reset = NULL', $modified_node_load_code);


  //
  // Modification of the node_save code!
  //
  $modified_node_save_code = '

  //************************ BEGIN OF PATCH ****************************
  if(aef_externodes_node_save($node))
    return;
  //************************ END OF PATCH ****************************



  // Let modules modify the node before it is saved to the database.
  node_invoke_nodeapi($node, \'presave\');
  global $user;

  $node->is_new = FALSE;

  // Apply filters to some default node fields:
  if (empty($node->nid)) {
    // Insert a new node.
    $node->is_new = TRUE;

    // When inserting a node, $node->log must be set because
    // {node_revisions}.log does not (and cannot) have a default
    // value.  If the user does not have permission to create
    // revisions, however, the form will not contain an element for
    // log so $node->log will be unset at this point.
    if (!isset($node->log)) {
      $node->log = \'\';
    }

    // For the same reasons, make sure we have $node->teaser and
    // $node->body.  We should consider making these fields nullable
    // in a future version since node types are not required to use them.
    if (!isset($node->teaser)) {
      $node->teaser = \'\';
    }
    if (!isset($node->body)) {
      $node->body = \'\';
    }
  }
  elseif (!empty($node->revision)) {
    $node->old_vid = $node->vid;
  }
  else {
    // When updating a node, avoid clobberring an existing log entry with an empty one.
    if (empty($node->log)) {
      unset($node->log);
    }
  }

  // Set some required fields:
  if (empty($node->created)) {
    $node->created = time();
  }
  // The changed timestamp is always updated for bookkeeping purposes (revisions, searching, ...)
  $node->changed = time();

  $node->timestamp = time();
  $node->format = isset($node->format) ? $node->format : FILTER_FORMAT_DEFAULT;

  // Generate the node table query and the node_revisions table query.
  if ($node->is_new) {
    _node_save_revision($node, $user->uid);
    drupal_write_record(\'node\', $node);
    db_query(\'UPDATE {node_revisions} SET nid = %d WHERE vid = %d\', $node->nid, $node->vid);
    $op = \'insert\';
  }
  else {
    drupal_write_record(\'node\', $node, \'nid\');
    if (!empty($node->revision)) {
      _node_save_revision($node, $user->uid);
      db_query(\'UPDATE {node} SET vid = %d WHERE nid = %d\', $node->vid, $node->nid);
    }
    else {
      _node_save_revision($node, $user->uid, \'vid\');
    }
    $op = \'update\';
  }

  // Call the node specific callback (if any).
  node_invoke($node, $op);
  node_invoke_nodeapi($node, $op);

  // Update the node access table for this node.
  node_access_acquire_grants($node);

  // Clear the page and block caches.
  cache_clear_all();

';

  runkit_function_redefine('node_save', '&$node', $modified_node_save_code);


  //
  // Modification of the node_delete code!
  //
  $modified_node_delete_code =  '

  $node = node_load($nid);

  if (node_access(\'delete\', $node)) {

    //************************ BEGIN OF PATCH ****************************
    if(aef_externodes_node_delete($node))
      return;
    //************************ END OF PATCH ****************************

    db_query(\'DELETE FROM {node} WHERE nid = %d\', $node->nid);
    db_query(\'DELETE FROM {node_revisions} WHERE nid = %d\', $node->nid);

    // Call the node-specific callback (if any):
    node_invoke($node, \'delete\');
    node_invoke_nodeapi($node, \'delete\');

    // Clear the page and block caches.
    cache_clear_all();

    // Remove this node from the search index if needed.
    if (function_exists(\'search_wipe\')) {
      search_wipe($node->nid, \'node\');
    }
    watchdog(\'content\', \'@type: deleted %title.\', array(\'@type\' => $node->type, \'%title\' => $node->title));
    drupal_set_message(t(\'@type %title has been deleted.\', array(\'@type\' => node_get_types(\'name\', $node), \'%title\' => $node->title)));
  }

';

  runkit_function_redefine('node_delete', '$nid', $modified_node_delete_code);



  if(module_exists('module_grants') && function_exists('get_current_revision_id'))
  {
    $modified_get_current_revision_id_code = '
    //************************ BEGIN OF PATCH ****************************
    if($vid = aef_externodes_get_current_revision_id($nid))
      return $vid;
    //************************ END OF PATCH ****************************
    return db_result(db_query(\'SELECT vid FROM {node} WHERE nid=%d\', $nid));
';

    runkit_function_redefine('get_current_revision_id', '$nid', $modified_get_current_revision_id_code);
  }


}
