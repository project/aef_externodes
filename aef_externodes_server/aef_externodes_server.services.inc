<?php


/**
 * Implementation of hook_service().
 */
function aef_externodes_server_service() {
  return array(
    // file.getwithoutdata
    array(
      '#method'           => 'file.getwithoutdata',
      '#callback'         => 'aef_externodes_server_get_file',
      '#access callback'  => 'aef_externodes_server_get_file_access',
      '#key'              => FALSE,
      '#file'             => array('file' => 'aef_externodes_server.services.inc', 'module' => 'aef_externodes_server'),
      '#args'             => array(
        array(
          '#name'           => 'fid',
          '#type'           => 'int',
          '#description'    => t('A file ID.'),
        ),
      ),
      '#return'           => 'array',
      '#help'             => t('The file data.')
    ),

    // views.getresultsandinfos
    array(
      '#method'           => 'views.getresultsandinfos',
      '#callback'         => 'aef_externodes_server_views_service_get',
      '#access callback'  => 'aef_externodes_server_views_service_get_access',
      '#file'             => array('file' => 'aef_externodes_server.services.inc', 'module' => 'aef_externodes_server'),
      '#key'              => FALSE,
      '#args'             => array(
        array(
          '#name'           => 'view_name',
          '#type'           => 'string',
          '#description'    => t('View name.')
        ),
        array(
          '#name'           => 'display',
          '#type'           => 'string',
          '#description'    => t('Display name.'),
          '#optional'       => TRUE,
        ),
        array(
          '#name'           => 'args',
          '#type'           => 'array',
          '#optional'       => TRUE,
          '#description'    => t('An array of arguments to pass to the view.')
        ),
        array(
          '#name'           => 'offset',
          '#type'           => 'int',
          '#optional'       => TRUE,
          '#description'    => t('An offset integer for paging.')
        ),
        array(
          '#name'           => 'limit',
          '#type'           => 'int',
          '#optional'       => TRUE,
          '#description'    => t('A limit integer for paging.')
        ),
        array(
          '#name'           => 'get_params',
          '#type'           => 'array',
          '#optional'       => TRUE,
          '#description'    => t('The _GET array. Used for AJAX pagination.')
        ),
      ),
      '#return'           => 'array',
      '#help'             => t('Retrieves a view defined in views.module.')
    ),

    // node.realsave
    array(
      '#method'           => 'node.realsave',
      '#callback'         => 'aef_externodes_server_node_service_save',
      '#access callback'  => 'aef_externodes_server_node_service_save_access',
      '#file'             => array('file' => 'aef_externodes_server.services.inc', 'module' => 'aef_externodes_server'),
      '#args'             => array(
        array(
          '#name'           => 'node',
          '#type'           => 'struct',
          '#description'    => t('A node object. Upon creation, node object must include "type". Upon update, node object must include "nid" and "changed".'))),
      '#return'           => 'array',
      '#help'             => t('Save a node object into the database.')
    ),

    // file.save
    array(
      '#method'           => 'file.save',
      '#callback'         => 'aef_externodes_server_file_service_save',
      '#access callback'  => 'aef_externodes_server_file_service_save_access',
      '#file'             => array('file' => 'aef_externodes_server.services.inc', 'module' => 'aef_externodes_server'),
      '#args'             => array(
        array(
          '#name'           => 'filename',
          '#type'           => 'string',
          '#description'    => t('The filename and filepath of the file to use.'),
        ),
        array(
          '#name'           => 'file',
          '#type'           => 'string',
          '#description'    => t('A file encoded in base64.'),
        ),
      ),
      '#return'           => 'array',
      '#help'             => t('The file data.')
    ),

    // Save a taxonomy vocabulary.
    array(
      '#method'   => 'taxonomy.saveVocabulary',
      '#callback' => 'aef_externodes_server_taxonomy_service_save_vocabulary',
      '#access callback'  => 'aef_externodes_server_taxonomy_access',
      '#file'             => array('file' => 'aef_externodes_server.services.inc', 'module' => 'aef_externodes_server'),
      '#args'     => array(
        array(
          '#name'         => 'vocabulary',
          '#type'         => 'array',
          '#description'  => t('A taxonomy vocabulary array, as would be returned by taxonomy_vocabulary_load().'),
        ),
      ),
      '#return'   => 'int',
      '#help'     => t('Save / create a taxonomy vocabulary. See taxonomy_save_vocabulary() in Drupal API for details.'),
    ),

    // Delete a taxonomy vocabulary.
    array(
      '#method'   => 'taxonomy.deleteVocabulary',
      '#callback' => 'aef_externodes_server_taxonomy_service_delete_vocabulary',
      '#access callback'  => 'aef_externodes_server_taxonomy_access',
      '#file'             => array('file' => 'aef_externodes_server.services.inc', 'module' => 'aef_externodes_server'),
      '#args'     => array(
        array(
          '#name'         => 'vocabulary',
          '#type'         => 'array',
          '#description'  => t('A taxonomy vocabulary array, as would be returned by taxonomy_vocabulary_load().'),
        ),
      ),
      '#return'   => 'int',
      '#help'     => t('Delete a taxonomy vocabulary. See taxonomy_save_vocabulary() in Drupal API for details.'),
    ),

    // Save a taxonomy term.
    array(
        '#method'   => 'taxonomy.saveTerm',
        '#callback' => 'aef_externodes_server_taxonomy_service_save_term',
        '#access callback'  => 'aef_externodes_server_taxonomy_access',
      '#file'             => array('file' => 'aef_externodes_server.services.inc', 'module' => 'aef_externodes_server'),
        '#args'     => array(
          array(
            '#name'         => 'term',
            '#type'         => 'array',
            '#description'  => t('A taxonomy term array, as would be returned by taxonomy_get_term().'),
          ),
        ),
        '#return'   => 'int',
        '#help'     => t('Save / create a taxonomy term. See taxonomy_save_term() in Drupal API for details.'),
    ),

    // taxonomy.deleteTerm.
    array(
        '#method'   => 'taxonomy.deleteTerm',
        '#callback' => 'aef_externodes_server_taxonomy_service_delete_term',
        '#access callback'  => 'aef_externodes_server_taxonomy_access',
        '#file'             => array('file' => 'aef_externodes_server.services.inc', 'module' => 'aef_externodes_server'),
        '#args'     => array(
          array(
            '#name'         => 'term',
            '#type'         => 'array',
            '#description'  => t('A taxonomy term array, as would be returned by taxonomy_get_term().'),
          ),
        ),
        '#return'   => 'int',
        '#help'     => t('Delete a taxonomy term. See taxonomy_save_term() in Drupal API for details.'),
    ),


    // taxonomy.eraseinstallall
    array(
      '#method'           => 'taxonomy.eraseAndInstallAll',
      '#callback'         => 'aef_externodes_server_taxonomy_erase_and_install_all',
      '#access callback'  => 'aef_externodes_server_taxonomy_access',
      '#file'             => array('file' => 'aef_externodes_server.services.inc', 'module' => 'aef_externodes_server'),
      '#args'             => array(
          array(
            '#name'           => 'data',
            '#type'           => 'array',
            '#description'    => t('An array of vocs, each vocs being an array of terms.'),
          ),
        ),
      '#return'           => 'int',
    ),

    // module_grants.get_current_revision_id
    array(
      '#method'           => 'module_grants.get_current_revision_id',
      '#callback'         => 'aef_externodes_get_current_revision_id',
      '#access callback'  => 'aef_externodes_get_current_revision_id_access',
      '#key'              => FALSE,
      '#file'             => array('file' => 'aef_externodes_server.services.inc', 'module' => 'aef_externodes_server'),
      '#args'             => array(
          array(
            '#name'           => 'nid',
            '#type'           => 'int',
            '#description'    => t('The nid.'),
          ),
        ),
      '#return'           => 'array',
    ),



  );
}


function aef_externodes_server_get_file($fid) {
  if ($file = db_fetch_array(db_query('SELECT * FROM {files} WHERE fid = %d', $fid))) {
    return $file;
  }
  else {
    return services_error(t('There is no file with the given ID.'));
  }
}

/**
 * Check if the user has permission to get a given file
 */
function aef_externodes_server_get_file_access($fid) {
  global $user;
  if (user_access('get any external files infos')) {
    return TRUE;
  }
}

/**
 * Get a view from the database.
 *
 * @param $view_name
 *   String. The views name.
 * @param $display_id
 *   String (optional).
 * @param $args
 *   Array (optional). A list of params.
 * @return
 *   Array. The views return.
 */
function aef_externodes_server_views_service_get($view_name, $display = "default", $args = array(), $offset = -1, $limit = -1, $get_params = array()) 
{
  global $pager_page_array, $pager_total, $pager_total_items;

  $view = views_get_view($view_name);

  $view->init_display();
  $view->set_display($display);
  $view->pre_execute();

  // Put all arguments and then execute
  $view->set_arguments($args, FALSE);

  if($offset >= 0)
    $view->set_offset($offset);

  if($limit >= 0)
    $view->set_items_per_page($limit);

  $_GET = $_GET + $get_params;


  $view->execute();

  return array(
    "results" => $view->result, 
    'pager_total_items' => $pager_total_items[$view->pager['element']],
    'pager_total' => $pager_total[$view->pager['element']],
    'pager_page_array' => $pager_page_array[$view->pager['element']]
    );
}

/**
 * Check the access permission to a given views.
 *
 * @param view_name
 *   String. The views name.
 * @return
 *   Boolean. TRUE if the user is allowed to load the given view.
 */
function aef_externodes_server_views_service_get_access($view_name) {
  $view = views_get_view($view_name);
  if (empty($view)) {
    return FALSE;
  }

  global $user;
  return views_access($view);
}

/**
 * Save a node. It creates a new one, in case the 'nid' field
 * is missing.
 *
 * @param $node The node that will be sent to node_save
 * @return
 *   Number. The node ID.
 */
function aef_externodes_server_node_service_save($node) {

  //Object-ize some parts of the node.
  $node = (object)$node;
  if(is_array($node->feedapi_object))
  {
    $node->feedapi_object = (object)$node->feedapi_object;
    if(is_array($node->feedapi_object->options))
      $node->feedapi_object->options = (object)$node->feedapi_object->options;
    if(is_array($node->feedapi_object->items))
    {
      foreach($node->feedapi_object->items as $delta => $item)
      {
        if(is_array($node->feedapi_object->items[$delta]['options']))
          $node->feedapi_object->items[$delta]['options'] = (object)$node->feedapi_object->items[$delta]['options'];
        $node->feedapi_object->items[$delta] = (object)$node->feedapi_object->items[$delta];
      }
    }
  }
  if(is_array($node->feedapi_node))
  {
    $node->feedapi_node = (object)$node->feedapi_node;
    if(isset($node->feedapi_node->feed_item))
    {
      $node->feedapi_node->feed_item = (object)$node->feedapi_node->feed_item;
      if(isset($node->feedapi_node->feed_item->options))
        $node->feedapi_node->feed_item->options = (object)$node->feedapi_node->feed_item->options;
    }
  }
  if(is_array($node->feedapi))
  {
    $node->feedapi = (object)$node->feedapi;
    if(isset($node->feedapi->feed_item))
    {
      $node->feedapi->feed_item = (object)$node->feedapi->feed_item;
      if(isset($node->feedapi->feed_item->options))
        $node->feedapi->feed_item->options = (object)$node->feedapi->feed_item->options;
    }
  }

  node_save($node);

  return array("nid" => $node->nid);
}

/**
 * Check if the user has the permission to save a node.
 *
 * @param $node
 *   Object. The node object.
 * @return
 *   Boolean. TRUE if the user has the permission to save a node.
 */
function aef_externodes_server_node_service_save_access($node) {
  if (isset($node->nid)) {
    return node_access('update', $node);
  }
  return node_access('create', $node['type']);
}


/**
 * Get all elements for a given file
 *
 * @param $filename File name and filepath
 *   Number. File ID
 * @return
 *   Array. All elements fomr a given file
 */
function aef_externodes_server_file_service_save($filename, $file) {
  global $user;
  $return = array();
  $dest = file_directory_path() . "/" . $filename;

  if (file_check_directory(dirname($dest), FILE_CREATE_DIRECTORY) && !$uploaded_filepath = file_save_data(base64_decode($file), $dest)) {
    return services_error(t('The file upload failed.' . $local_filename . "--" . $dest ));
  }
  else
  {
    //Ok, file uploaded! Create the file object
    $parts = explode('/', $uploaded_filepath);
    $uploaded_filepath_name = array_pop($parts);

    $new_file->uid = $user->uid;
    $new_file->filepath = $uploaded_filepath;
    $new_file->filename = $uploaded_filepath_name;
    $new_file->filemime = file_get_mimetype($file->filename);
    $new_file->filesize = filesize($uploaded_filepath);
    $new_file->timestamp = time();
    $new_file->status = FILE_STATUS_PERMANENT;
    drupal_write_record('files', $new_file);
    $return['fid'] = $new_file->fid;
  }

  return $return;
}

/**
 * Check if the user has permission to get a given file
 */
function aef_externodes_server_file_service_save_access($fid) {
  global $user;
  if (user_access('save any binary files')) {
    return TRUE;
  }
  return false;
}

/**
 * Services interface to taxonomy_save_term().
 *
 * @see
 *   taxonomy_save_term().
 */
function aef_externodes_server_taxonomy_service_save_term($term) {
  if (!is_array($term)) {
    $term = (array) $term;
  }
  aef_externodes_write_record_and_key('term_data', $term);
  return taxonomy_save_term($term);
}

function aef_externodes_server_taxonomy_service_delete_term($term) {
  if (!is_array($term)) {
    $term = (array) $term;
  }
  return taxonomy_del_term($term['tid']);
}

/**
 * Services interface to taxonomy_save_vocabulary().
 *
 * @see
 *   taxonomy_save_vocabulary().
 */
function aef_externodes_server_taxonomy_service_save_vocabulary($vocabulary) {
  if (!is_array($vocabulary)) {
    $vocabulary = (array) $vocabulary;
  }
  aef_externodes_write_record_and_key('vocabulary', $vocabulary);
  return taxonomy_save_vocabulary($vocabulary);
}

function aef_externodes_server_taxonomy_service_delete_vocabulary($vocabulary) {
  if (!is_array($vocabulary)) {
    $vocabulary = (array) $vocabulary;
  }
  return taxonomy_del_vocabulary($vocabulary['vid']);
}

function aef_externodes_server_taxonomy_erase_and_install_all($data) {

  $vocs = taxonomy_get_vocabularies();
  foreach($vocs as $vid => $voc)
  {
    $terms = taxonomy_get_tree($vid);
    foreach($terms as $delta => $term)
    {
      taxonomy_del_term($term->tid);
    }
    taxonomy_del_vocabulary($vid);
  }

  foreach($data as $vid => $voc)
  {
    $terms = $voc['terms'];
    aef_externodes_write_record_and_key('vocabulary', $voc);
    taxonomy_save_vocabulary($voc);

    foreach($terms as $delta => $term)
    {
      aef_externodes_write_record_and_key('term_data', $term);
      taxonomy_save_term($term);
    }
  }

  return 1;
}

/**
 * Based on drupal_write_record. It makes full inserts, even the serial key!
 */
function aef_externodes_write_record_and_key($table, &$object)
{
  $schema = drupal_get_schema($table);
  if (empty($schema)) {
    return FALSE;
  }

  // Convert to an object if needed.
  if (is_array($object)) {
    $object = (object) $object;
    $array = TRUE;
  }
  else {
    $array = FALSE;
  }

  $fields = $defs = $values = $serials = $placeholders = array();

  // Go through our schema, build SQL, and when inserting, fill in defaults for
  // fields that are not set.
  foreach ($schema['fields'] as $field => $info) {
    // For inserts, populate defaults from Schema if not already provided
    if (!isset($object->$field) && !count($update) && isset($info['default'])) {
      $object->$field = $info['default'];
    }

    // Build arrays for the fields, placeholders, and values in our query.
    if (isset($object->$field)) {
      $fields[] = $field;
      $placeholders[] = db_type_placeholder($info['type']);

      if (empty($info['serialize'])) {
        $values[] = $object->$field;
      }
      else {
        $values[] = serialize($object->$field);
      }
    }
  }

  $query = "INSERT INTO {". $table ."} (". implode(', ', $fields) .') VALUES ('. implode(', ', $placeholders) .')';

  // Execute the SQL.
  if (db_query($query, $values)) {
    if ($serials) {
      // Get last insert ids and fill them in.
      foreach ($serials as $field) {
        $object->$field = db_last_insert_id($table, $field);
      }
    }
  }
  else {
    $return = FALSE;
  }

  // If we began with an array, convert back so we don't surprise the caller.
  if ($array) {
    $object = (array) $object;
  }


  return true;
}

/**
 *
 */
function aef_externodes_server_taxonomy_access() {
  global $user;
  if (user_access('administer taxonomy')) {
    return TRUE;
  }
  return false;
}

function aef_externodes_get_current_revision_id($nid)
{
  return array('vid' => db_result(db_query('SELECT vid FROM {node} WHERE nid=%d', $nid)));
}

function aef_externodes_get_current_revision_id_access() {
  global $user;
  if (user_access('access content')) {
    return TRUE;
  }
  return false;
}
